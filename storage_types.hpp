//
// Created by User on 13.12.2019.
//

#ifndef SIECI_STORAGE_TYPES_HPP
#define SIECI_STORAGE_TYPES_HPP

#include <cstdio>
#include <deque>





//usunonci gdy michau g napisze
class Package{};




using stock=std::deque<Package>;
enum PackageQueueType{LIFO,FIFO};
class IPackageStockpile{
protected:
    stock stockpile={};
public:
    void push(Package&& p);
    void push(const Package& p);
    bool empty() const;
    std::size_t size() const;
    const Package& operator[] (std::size_t index) const;
    const stock::const_iterator cbegin() const;
    const stock::const_iterator cend() const;
    const stock::const_iterator begin() const;
    const stock::const_iterator end() const;
};
class IPackageQueue:IPackageStockpile{
    virtual Package pop();
    virtual PackageQueueType get_queue_type() const;
};
#endif //SIECI_STORAGE_TYPES_HPP
