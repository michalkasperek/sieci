#include "storage_types.hpp"

std::size_t IPackageStockpile::size() const{
    return stockpile.size();
}

bool IPackageStockpile::empty() const{
    return stockpile.size()==0;
}

void IPackageStockpile::push(Package&& p) {
    stockpile.push_back(p);
}

void IPackageStockpile::push(const Package& p) {
    stockpile.push_back(p);
}

const Package& IPackageStockpile::operator[] (std::size_t index) const {return stockpile[index];}

const stock::const_iterator IPackageStockpile::cbegin() const{return stockpile.cbegin();}

const stock::const_iterator IPackageStockpile::cend() const{ return  stockpile.cend();}

const stock::const_iterator IPackageStockpile::begin() const{return stockpile.cbegin();}

const stock::const_iterator IPackageStockpile::end() const{ return  stockpile.cend();}
